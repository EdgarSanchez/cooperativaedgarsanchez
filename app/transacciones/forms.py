from django import forms
from app.modelo.models import Banco
from app.modelo.models import Transaccion
from app.modelo.models import BancaVirtual
from app.modelo.models import Caja
from app.modelo.models import CajeroAutomatico, Transferencia


class FormularioTransaccion(forms.ModelForm):
    class Meta:
        model = Transaccion
        fields = ["tipo", "valor", "descripcion"]


class FormularioTransferencia(forms.ModelForm):
    class Meta:
        model = Transferencia
        fields = [ "valor", "descripcion"]


class FormularioBancaVirtual(forms.ModelForm):
    class Meta:
        model = BancaVirtual
        fields = ["numeroCuentaDestino", "dniTitularCuentaDestino",
                  "titularCuentaDestino", "transaccion"]


class FormularioCaja(forms.ModelForm):
    class Meta:
        model = Caja
        fields = ["nombres", "apellidos", "transaccion"]


class FormularioCajeroAutomatico(forms.ModelForm):
    class Meta:
        model = CajeroAutomatico
        fields = ["codigo", "ubicacion", "transaccion"]
