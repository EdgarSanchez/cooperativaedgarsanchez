from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .forms import FormularioTransaccion, FormularioTransferencia
from app.modelo.models import Cliente, Cuenta, Transaccion, Transferencia
from app.clientes.views import crear
import random
from django.contrib.auth.decorators import login_required


@login_required
def transaccion(request):
    usuario = request.user
    if usuario.has_perm('modelo.add_transaccion'):

        numero = request.GET['numero']
        cuenta = Cuenta.objects.get(numero=numero)
        formulario = FormularioTransaccion(request.POST)
        context = {
            'f': formulario,
            'numero': numero,
            'title': "Transaccion",
            'mensaje': "Ingresar Nueva Transaccion"
        }

        if request.method == 'POST':
            if formulario.is_valid():
                datos = formulario.cleaned_data
                transaccion = Transaccion()

                transaccion.cuenta = cuenta
                transaccion.tipo = datos.get('tipo')
                transaccion.valor = datos.get('valor')
                transaccion.descripcion = datos.get('descripcion')

                cuenta = Cuenta.objects.get(numero=numero)
                cuentaSuma = Cuenta.objects.filter(numero=numero)

                suma = 0

                for item in cuentaSuma:
                    if datos.get('tipo') == 'deposito':
                        suma = datos.get('valor') + item.saldo
                    if datos.get('tipo') == 'retiro':
                        if item.saldo >= datos.get('valor'):
                            suma = item.saldo - datos.get('valor')
                        else:
                            messages.warning(request, 'Saldo Insuficiente')
                            return render(request, 'transacciones/Transaccion.html', context)
                cuenta.saldo = suma

                transaccion.save()
                cuenta.save()
                messages.warning(request, 'Realizado exitosamente')
                context = {
                    'fecha': transaccion.fecha,
                    'tipo': transaccion.tipo,
                    'valor': transaccion.valor,
                    'cuenta': transaccion.cuenta,
                    'saldo': cuenta.saldo,
                    'descripcion': transaccion.descripcion
                }
                return render(request, 'transacciones/Presentar.html', context)
        numero = random.randrange(10)

        return render(request, 'transacciones/Transaccion.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')


@login_required()
def verTransaccion(request):
    usuario = request.user
    if usuario.has_perm('modelo.add_cliente'):
        dni = request.GET['numero']
        c = Cuenta.objects.get(numero=dni)
        listaClientes = Transaccion.objects.filter(cuenta=c)
        context = {
            'title': "LISTA DE TRANSACCIONES DE: "+dni,
            'numero': dni,
            'lista': listaClientes,
        }
        return render(request, 'transacciones/ver_transaccion.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')


@login_required
def buscarCuenta1(request):
    buscarCuenta1 = request.POST['buscarCuenta1']
    listaBuscaCuenta1 = Cuenta.objects.filter(numero=buscarCuenta1)
    print(listaBuscaCuenta1)
    messages.warning(request, 'Cuenta 1 encontrado')
    buscarCuenta2 = request.POST['buscarCuenta2']
    listaBuscaCuenta2 = Cuenta.objects.filter(numero=buscarCuenta2)
    print(listaBuscaCuenta2)
    messages.warning(request, 'Cuenta 2 encontrado')

    return render(request, 'transacciones/crear_Transferencia.html')


def buscarTrans(request):
    usuario = request.user
    if usuario.has_perm('modelo.view_cliente'):
        numero = request.GET['numero']
        context = {

            'title': "TRANSAFERENCIA",
            'cliente': numero,
            'permisoEditar': usuario.has_perm('modelo.change_cuenta'),
            'permisoCrear': usuario.has_perm('modelo.add_cuenta'),
        }
        return render(request, 'transacciones/buscar.html', context)
    else:
        return redirect('home_page.html')


@login_required
def crearTransferencia(request):
    usuario = request.user
    if usuario.has_perm('modelo.add_cliente'):
        numero = request.GET['numero']
        destino = request.GET['txt_buscar']
        context = {
            'title': "TRANSAFERENCIA",
            'cliente': numero,
            'permisoEditar': usuario.has_perm('modelo.change_cuenta'),
            'permisoCrear': usuario.has_perm('modelo.add_cuenta'),
        }
        try:
            cuenta2 = Cuenta.objects.get(numero=destino)
            cuenta1 = Cuenta.objects.get(numero=numero)
            if cuenta1 == cuenta2:
                messages.warning(request, 'Misma Cuenta / Ingrese una diferente')
                return render(request, 'transacciones/buscar.html', context)
            formulario = FormularioTransferencia(request.POST)
            context = {
                'f': formulario,
                'cuenta1': cuenta1,
                'cuenta2': cuenta2,
                'title': "Ingresar Transaccion",
                'mensaje': "Ingresar Nueva Tranferencia"
            }
            if request.method == 'POST':
                if formulario.is_valid():
                    datos = formulario.cleaned_data
                    transferencia = Transferencia()
                    transferencia.cuenta1 = cuenta1
                    transferencia.cuenta2 = cuenta2
                    transferencia.valor = datos.get('valor')
                    transferencia.descripcion = datos.get('descripcion')

                    cuentaSuma = Cuenta.objects.filter(numero=numero)
                    suma = 0
                    for item in cuentaSuma:
                        if item.saldo >= datos.get('valor'):
                            suma = item.saldo - datos.get('valor')
                        else:
                            messages.warning(request, 'Saldo insuficiente')
                            return render(request, 'transacciones/Transaccion.html', context)
                    cuenta1.saldo = suma

                    cuentaSuma2 = Cuenta.objects.filter(numero=destino)
                    for item in cuentaSuma2:
                        suma = item.saldo + datos.get('valor')
                    cuenta2.saldo = suma

                    transferencia.save()
                    cuenta1.save()
                    cuenta2.save()
                    messages.warning(request, 'Transferencia exitosa')
                    context = {
                        'fecha': transferencia.fecha,
                        'valor': transferencia.valor,
                        'cuenta1': transferencia.cuenta1,
                        'cuenta2': transferencia.cuenta2,
                        'descripcion': transferencia.descripcion
                    }
                    return render(request, 'transacciones/Presentar_Transferencia.html', context)
        except:
            messages.warning(request, 'Cuenta no existente')
            return render(request, 'transacciones/buscar.html', context)

        numero = random.randrange(10)
        messages.warning(request, 'Complete los campos')
        return render(request, 'transacciones/crear_Transferencia.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')
