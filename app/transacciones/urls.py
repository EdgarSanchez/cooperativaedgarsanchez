from django.urls import path

from . import views

urlpatterns = [
    path('transaccion', views.transaccion),
    path('verTransaccion', views.verTransaccion),
    path('buscar1', views.buscarCuenta1),
    path('transferencia', views.crearTransferencia),
    path('buscarCuentaTransferencia', views.buscarTrans),
    
]
