from django.db import models

# Create your models here.
"""
Modelo de Cliente que posee cuentas
"""
class Cliente(models.Model):

    listaGenero = (
        ('femenino', 'Femenino'),
        ('masculino', 'Masculino'),
        (None, 'Elija una opción')
    )
    listaEstadoCivil = (
        ('soltero', 'Soltero/a'),
        ('casado', 'Casado/a'),
        ('viudo', 'Viudo/a'),
        ('divorciado', 'Divorciado/a'),
        ('unionLibre', 'Unión Libre'),
        (None, 'Elija una opción')
    )

    cliente_id = models.AutoField(primary_key=True)
    cedula = models.CharField(unique=True, max_length=10, null=False)
    nombres = models.CharField(max_length=70, null=False)
    apellidos = models.CharField(max_length=70, null=False)
    genero = models.CharField(max_length=15, choices=listaGenero, null=False, blank=False)
    estadoCivil = models.CharField(
        max_length=15, choices=listaEstadoCivil, null=False)
    fechaNacimiento = models.DateField(
        auto_now=False, auto_now_add=False, null=False)
    correo = models.EmailField(unique=True, max_length=100, null=False)
    telefono = models.CharField(max_length=7, null=False)
    celular = models.CharField(max_length=10, null=False)
    direccion = models.TextField(null=False)
    #uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.cedula


class Banco(models.Model):

    nombre = models.CharField(primary_key=True, max_length=25)
    direccion = models.CharField(max_length=225)
    telefono = models.CharField(max_length=10)
    correo = models.EmailField(max_length=200)


class Cuenta(models.Model):

    listaTipo = (
        ('corriente', 'Corriente'),
        ('ahorros', 'Ahorro'),
        ('basica', 'Básica'),
        ('nomina', 'Nómina'),
        ('valores', 'Valores'),
        ('juvenil', 'Juvenil'),
        ('programado', 'Ahorro Programado'),
        ('euros', 'Ahorro en Euros'),
        (None, 'Elija una opción')
    )
    cuenta_id = models.AutoField(primary_key=True)
    numero = models.CharField(max_length=20, unique=True, null=False)
    estado = models.BooleanField(null=False, default=True)
    fechaApertura = models.DateField(auto_now_add=True, null=False)
    saldo = models.DecimalField(max_digits=10, decimal_places=3, null=False)
    tipoCuenta = models.CharField(max_length=30, choices=listaTipo, null=False)
    cliente = models.ForeignKey(
        'Cliente',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.numero


class Transaccion(models.Model):

    listaTipoC = (
        ('retiro', 'Retiro'),
        ('deposito', 'Depósito'),
        (None, 'Eliga una opción')
    )
    transaccion_id = models.AutoField(primary_key=True)
    fecha = models.DateTimeField(auto_now_add=True, null=False)
    tipo = models.CharField(max_length=30, choices=listaTipoC, null=False)
    valor = models.DecimalField(max_digits=10, decimal_places=3, null=False)
    descripcion = models.TextField(null=False)
    cuenta = models.ForeignKey(
        'Cuenta',
        on_delete=models.CASCADE,
    )


class BancaVirtual (models.Model):
    bancaVirtual_id = models.AutoField(primary_key=True)
    numeroCuentaDestino = models.CharField(
        max_length=20, unique=True, null=False)
    dniTitularCuentaDestino = models.CharField(
        unique=True, max_length=10, null=False)
    titularCuentaDestino = models.CharField(max_length=160, null=False)
    transaccion = models.ForeignKey(
        'Transaccion',
        on_delete=models.CASCADE,
    )


class Caja (models.Model):
    caja_id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=70, null=False)
    apellidos = models.CharField(max_length=70, null=False)
    transaccion = models.ForeignKey(
        'Transaccion',
        on_delete=models.CASCADE,
    )


class CajeroAutomatico (models.Model):
    cajeroAutomatico_id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=15, null=False)
    ubicacion = models.TextField(null=False)
    transaccion = models.ForeignKey(
        'Transaccion',
        on_delete=models.CASCADE,
    )


class Transferencia(models.Model):

    transferencia_id = models.AutoField(primary_key=True)
    fecha = models.DateTimeField(auto_now_add=True, null=False)
    valor = models.DecimalField(max_digits=10, decimal_places=3, null=False)
    descripcion = models.TextField(null=False)
    cuenta1 = models.ForeignKey(
        'Cuenta',
        related_name='cuenta1',
        on_delete=models.CASCADE,
    )
    cuenta2 = models.ForeignKey(
        'Cuenta',
        related_name='cuenta2',
        on_delete=models.CASCADE,
    )
