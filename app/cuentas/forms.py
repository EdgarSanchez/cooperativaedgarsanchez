from django import forms
from app.modelo.models import Cuenta

class FormularioCuenta(forms.ModelForm):
    class Meta:
        model = Cuenta
        fields = [ "cliente", "tipoCuenta","saldo"]