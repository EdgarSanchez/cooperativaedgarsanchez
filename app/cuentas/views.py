from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .forms import FormularioCuenta
from app.modelo.models import Cuenta
import random
from django.contrib.auth.decorators import login_required
# Create your views here.
def principalCuenta(request):
    usuario = request.user
    if usuario.has_perm('modelo.add_cliente'):
        listaCuentas = Cuenta.objects.all()
        context = {
            'cuentas': listaCuentas,
            'title': "Cuentas",
            'mensaje': "Modulo Cuentas"
        }
        return render(request, 'cuentas/principal_cuenta.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')


@login_required
def crearCuenta(request):
    usuario = request.user
    if usuario.has_perm('modelo.change_cliente'):
        usuario = request.user
        if usuario.has_perm('modelo.add_cliente'):
            formulario = FormularioCuenta(request.POST)
            if request.method == 'POST':
                if formulario.is_valid():
                    numero = random.randint(1000000000,9999999999)
                    num = numero.__str__()
                    while Cuenta.objects.filter(numero = num).exists():
                        numero = random.randrange(10)

                    datos = formulario.cleaned_data
                    cuenta = Cuenta()
                    cuenta.numero = num
                    cuenta.saldo = datos.get('saldo')
                    cuenta.cliente = datos.get('cliente')
                    cuenta.tipoCuenta = datos.get('tipoCuenta')
                    cuenta.save()
                    messages.warning(request, 'Guardado Exitosamente')
                    return redirect(principalCuenta)
            context = {
                'f': formulario,
                'title': "Ingresar Cuenta",
                'mensaje': "Ingresar nueva Cuenta"
            }
            return render(request, 'cuentas/crear_cuenta.html', context)
        else:
            messages.warning(request, 'No Permitido')
            return render(request, 'login/403.html')


def buscarCuentas(request):
    buscarCuenta = request.POST['buscarCuenta']
    listaBuscaNumero = Cuenta.objects.filter(numero=buscarCuenta)
    print(listaBuscaNumero)
    context = {
        'cuentas': listaBuscaNumero,
        'title': "Cuentas",
        'mensaje': "Modulo Cuentas",
        'cuenta': True
    }
    if listaBuscaNumero:
        messages.warning(request, 'Cuenta encontrada')
        return render(request, 'cuentas/buscar_cuentas.html', context)
    else:
        messages.warning(request, 'No Existe')
        return render(request, 'cuentas/buscar_cuentas.html', context)
