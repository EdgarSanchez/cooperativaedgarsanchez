from django.urls import path

from . import views

urlpatterns = [
    path('', views.principalCuenta, name = 'cuentas'),
    path('crearCuenta', views.crearCuenta),
    path('buscar', views.buscarCuentas)
]